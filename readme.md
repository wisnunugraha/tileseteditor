
# Simple Tileset Editor

## Authors

- [@wisnunugraha](https://www.gitlab.com/wisnunugraha)


## Run This Project

Run this Editor

clone this project and run npm install
```bash
  clone this project and cd to project
```

```bash
  open file index.html
```

## Support

For support, email wisnunugraha@yahoo.com or join our Slack channel.

## Tech Stack

**Client:** HTML

**Server:** JavaScript

